package com.vicros0723.fudget.view.controller;

import com.vicros0723.fudget.application.event.Event;
import com.vicros0723.fudget.application.event.OpenFudgetException;
import com.vicros0723.fudget.application.event.SaveFudgetException;
import com.vicros0723.fudget.domain.account.Account;
import com.vicros0723.fudget.view.control.AccountCreationDialog;
import com.vicros0723.fudget.view.control.NewFudgetConfirmation;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.FileChooser;

import java.io.File;

import static com.vicros0723.fudget.application.action.Actionable.*;
import static javafx.scene.input.KeyCode.*;
import static javafx.scene.input.KeyCombination.SHORTCUT_DOWN;

public class MenuBarController extends AbstractController {
    @FXML
    private MenuBar root;
    @FXML
    private MenuItem newFudget;
    @FXML
    private MenuItem openFudget;
    @FXML
    private MenuItem saveFudget;

    public void initialize() {
        root.setUseSystemMenuBar(true);
        newFudget.setAccelerator(new KeyCodeCombination(N, SHORTCUT_DOWN));
        openFudget.setAccelerator(new KeyCodeCombination(O, SHORTCUT_DOWN));
        saveFudget.setAccelerator(new KeyCodeCombination(S, SHORTCUT_DOWN));
    }

    @Override
    public void onNext(final Event event) {
        if (event instanceof Event.Failure) {
            if (((Event.Failure) event).getException() instanceof SaveFudgetException) {
                Platform.runLater(new Alert(Alert.AlertType.ERROR, "Unable to save the Fudget", ButtonType.OK)::showAndWait);
            }
            if (((Event.Failure) event).getException() instanceof OpenFudgetException) {
                Platform.runLater(new Alert(Alert.AlertType.ERROR, "Unable to open the Fudget", ButtonType.OK)::showAndWait);
            }
        }
        super.onNext(event);
    }

    public void newFudget() {
        final Alert confirmation = new NewFudgetConfirmation();
        confirmation.showAndWait()
                .filter(buttonType -> buttonType == ButtonType.OK)
                .ifPresent(buttonType -> dispatch(new NewFudget()));
    }

    public void openFudget() {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select fudget to open");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Fudget Files(*.fudget)", "*.fudget"));
        final File file = fileChooser.showOpenDialog(root.getScene().getWindow());
        if (file == null) {
            return;
        }
        dispatch(new OpenFudget(file));
    }

    public void saveFudget() {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save fudget");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Fudget Files (*.fudget)", "*.fudget"));
        final File file = fileChooser.showSaveDialog(root.getScene().getWindow());
        if (file == null) {
            return;
        }
        dispatch(new SaveFudget(file));
    }

    public void createAccount() {
        final Dialog<Account> dialog = new AccountCreationDialog();
        dialog.showAndWait()
                .ifPresent(account -> dispatch(new CreateAccount(account)));
    }


}
