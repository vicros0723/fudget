package com.vicros0723.fudget.view.control;

import com.vicros0723.fudget.domain.account.Account;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import static javafx.scene.control.ButtonBar.ButtonData;

public class AccountCreationDialog extends Dialog<Account> {
    public AccountCreationDialog() {
        final DialogPane dialogPane = getDialogPane();

        final Label nameLabel = new Label("Name: ");
        final TextField nameTextField = new TextField();
        Platform.runLater(nameTextField::requestFocus);

        final Label typeLabel = new Label("Type: ");
        final ChoiceBox<Account.Type> typeChoiceBox = new ChoiceBox<>();
        typeChoiceBox.getItems().addAll(Account.Type.CHECKING, Account.Type.SAVING, Account.Type.CREDIT_CARD);
        typeChoiceBox.getSelectionModel().select(Account.Type.CHECKING);

        final Label onBudgetLabel = new Label("On budget?");
        final CheckBox onBudgetCheckBox = new CheckBox();
        onBudgetCheckBox.setSelected(true);

        final GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setAlignment(Pos.CENTER_LEFT);
        grid.add(nameLabel, 0, 0);
        grid.add(nameTextField, 1, 0);
        grid.add(typeLabel, 0, 1);
        grid.add(typeChoiceBox, 1, 1);
        grid.add(onBudgetLabel, 0, 2);
        grid.add(onBudgetCheckBox, 1, 2);

        setTitle("Create an account");
        dialogPane.setContent(grid);
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        final Button button = (Button) getDialogPane().lookupButton(ButtonType.OK);
        button.addEventFilter(ActionEvent.ACTION, event -> {
            if (nameTextField.getText().isEmpty() || typeChoiceBox.getValue() == null) {
                event.consume();
            }
        });

        setResultConverter(dialogButton -> {
            if (dialogButton.getButtonData() == ButtonData.OK_DONE) {
                return new Account(nameTextField.getText(), typeChoiceBox.getValue(), onBudgetCheckBox.isSelected());
            }
            return null;
        });
    }
}
