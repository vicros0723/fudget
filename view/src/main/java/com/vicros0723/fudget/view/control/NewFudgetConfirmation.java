package com.vicros0723.fudget.view.control;

import javafx.scene.control.Alert;

public class NewFudgetConfirmation extends Alert {
    public NewFudgetConfirmation() {
        super(AlertType.CONFIRMATION);
        setTitle("Confirm new Fudget");
        setContentText("You will lose any unsaved changes. Are you sure you want to start a new fudget?");
    }
}
