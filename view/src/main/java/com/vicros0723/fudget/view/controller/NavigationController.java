package com.vicros0723.fudget.view.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;

public class NavigationController extends AbstractController {
    @FXML
    private Accordion root;

    public void initialize() {
        root.expandedPaneProperty().addListener((observable, oldValue, newValue) -> {
            if (root.getPanes().stream().anyMatch(TitledPane::isExpanded)) {
                return;
            }
            if (oldValue != null) {
                root.setExpandedPane(oldValue);
            }
        });
    }
}
