package com.vicros0723.fudget.view;

import com.vicros0723.fudget.application.manager.FudgetManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(final Stage primaryStage) throws Exception {
        final FudgetManager fudgetManager = FudgetManager.getInstance();
        Parent layout = FXMLLoader.load(getClass().getResource("fxml/Layout.fxml"));
        primaryStage.setTitle("Fudget");
        primaryStage.setScene(new Scene(layout, 800, 600));
        primaryStage.show();
        fudgetManager.init();
    }

    public static void main(final String[] args) {
        launch(args);
    }
}
