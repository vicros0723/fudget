package com.vicros0723.fudget.view.controller;

import com.vicros0723.fudget.application.event.Event;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class LayoutController extends AbstractController {
    @FXML
    private BorderPane root;

    @Override
    public void onNext(final Event event) {
        if (event instanceof Event.Success) {
            Event.Success successEvent = (Event.Success) event;
            Platform.runLater(() -> root.setCenter(new Button(successEvent.getFudget().getSelectedAccount().toString())));
        }
        super.onNext(event);
    }
}
