package com.vicros0723.fudget.view.controller;

import com.vicros0723.fudget.application.event.Event;
import com.vicros0723.fudget.domain.account.Account;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

import java.util.ArrayList;
import java.util.List;

import static com.vicros0723.fudget.application.action.Actionable.SelectAccount;

public class AccountsController extends AbstractController {
    @FXML
    private ListView<Account> root;

    public void initialize() {
        root.getSelectionModel().selectedItemProperty().addListener((observable, oldAccount, newAccount) -> {
            if (newAccount != null) {
                dispatch(new SelectAccount(newAccount));
            }
        });
    }

    @Override
    public void onNext(final Event event) {
        if (event instanceof Event.Success) {
            Event.Success successEvent = (Event.Success) event;
            Platform.runLater(() -> {
                // This order of operations selects the new account before removing a possibly selected account
                // and prevents an infinite selection loop
                final List<Account> accounts = new ArrayList<>(successEvent.getFudget().getAccounts());
                accounts.removeAll(root.getItems());
                root.getItems().addAll(accounts);

                root.getSelectionModel().select(successEvent.getFudget().getSelectedAccount());

                root.getItems().retainAll(successEvent.getFudget().getAccounts());
            });
        }
        super.onNext(event);
    }

}
