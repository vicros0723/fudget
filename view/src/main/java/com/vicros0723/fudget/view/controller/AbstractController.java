package com.vicros0723.fudget.view.controller;

import com.vicros0723.fudget.application.action.Actionable;
import com.vicros0723.fudget.application.event.EventPublisher;
import com.vicros0723.fudget.application.event.Event;
import com.vicros0723.fudget.application.manager.FudgetManager;

import java.util.concurrent.Flow;

abstract class AbstractController implements Flow.Subscriber<Event> {
    private Flow.Subscription subscription;
    private FudgetManager fudgetManager;

    AbstractController() {
        EventPublisher.getInstance().subscribe(this);
        fudgetManager = FudgetManager.getInstance();
    }

    void dispatch(final Actionable actionable) {
        fudgetManager.manage(actionable);
    }

    @Override
    public void onSubscribe(final Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onNext(final Event event) {
        subscription.request(1);
    }

    @Override
    public void onError(final Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
    }
}
