package com.vicros0723.fudget.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vicros0723.fudget.domain.account.Account;

import java.util.List;

public class Fudget {
    public static final Fudget DEFAULT_FUDGET = new Fudget(List.of(Account.ALL_ACCOUNTS), Account.ALL_ACCOUNTS);

    private final List<Account> accounts;
    private final Account selectedAccount;

    @JsonCreator
    public Fudget(@JsonProperty("accounts") final List<Account> accounts,
                  @JsonProperty("selectedAccount") final Account selectedAccount) {
        this.accounts = accounts;
        this.selectedAccount = selectedAccount;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Account getSelectedAccount() {
        return selectedAccount;
    }

    public Fudget withAccounts(final List<Account> accounts) {
        return new Fudget(accounts, selectedAccount);
    }

    public Fudget withSelectedAccount(final Account selectedAccount) {
        return new Fudget(accounts, selectedAccount);
    }
}
