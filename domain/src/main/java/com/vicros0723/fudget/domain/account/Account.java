package com.vicros0723.fudget.domain.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Account {
    public static final Account ALL_ACCOUNTS = new Account("All Accounts", Account.Type.CHECKING, false);

    public enum Type {
        CREDIT_CARD, CHECKING, SAVING
    }

    private String name;
    private Type type;
    private boolean onBudget;

    @JsonCreator
    public Account(@JsonProperty("name") final String name,
                   @JsonProperty("type") final Type type,
                   @JsonProperty("onBudget") final boolean onBudget) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Account must have a name");
        }
        if (type == null) {
            throw new IllegalArgumentException("Account must have a type");
        }
        this.name = name;
        this.type = type;
        this.onBudget = onBudget;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public boolean isOnBudget() {
        return onBudget;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Account account = (Account) o;
        return name.equals(account.name) &&
                type == account.type &&
                onBudget == account.onBudget;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, onBudget);
    }

    @Override
    public String toString() {
        return name;
    }
}
