package com.vicros0723.fudget.domain.account;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class AccountTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIfNullName() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Account must have a name");
        new Account(null, Account.Type.CHECKING, true);
    }

    @Test
    public void shouldThrowIfEmptyName() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Account must have a name");
        new Account("", Account.Type.CHECKING, true);
    }

    @Test
    public void shouldThrowIfNullType() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Account must have a type");
        new Account("Name", null, true);
    }
}
