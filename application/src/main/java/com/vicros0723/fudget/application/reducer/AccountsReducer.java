package com.vicros0723.fudget.application.reducer;

import com.vicros0723.fudget.application.action.Actionable;
import com.vicros0723.fudget.application.action.Actionable.CreateAccount;
import com.vicros0723.fudget.application.action.Actionable.NewFudget;
import com.vicros0723.fudget.application.action.Actionable.SelectAccount;
import com.vicros0723.fudget.domain.Fudget;
import com.vicros0723.fudget.domain.account.Account;

import java.util.ArrayList;
import java.util.List;

import static com.vicros0723.fudget.domain.Fudget.DEFAULT_FUDGET;

class AccountsReducer {
    List<Account> calculateNewAccounts(final Fudget fudget, final Actionable actionable) {
        if (actionable instanceof NewFudget) {
            return DEFAULT_FUDGET.getAccounts();
        }
        if (actionable instanceof CreateAccount) {
            final List<Account> newAccounts = new ArrayList<>(fudget.getAccounts());
            newAccounts.add(((CreateAccount) actionable).getAccount());
            return newAccounts;
        }
        return fudget.getAccounts();
    }

    Account calculateNewSelectedAccount(final Fudget fudget, final Actionable actionable) {
        if (actionable instanceof NewFudget) {
            return DEFAULT_FUDGET.getSelectedAccount();
        }
        if (actionable instanceof CreateAccount) {
            return ((CreateAccount) actionable).getAccount();
        }
        if (actionable instanceof SelectAccount) {
            return ((SelectAccount) actionable).getAccount();
        }
        return fudget.getSelectedAccount();
    }
}
