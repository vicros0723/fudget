package com.vicros0723.fudget.application.action;

import java.io.File;

public interface LastOpenFudgetFilePreferenceSettable {
    File getFile();
}
