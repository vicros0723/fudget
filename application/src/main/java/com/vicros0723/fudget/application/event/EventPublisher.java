package com.vicros0723.fudget.application.event;

import java.util.concurrent.SubmissionPublisher;

public class EventPublisher extends SubmissionPublisher<Event> {
    private static final EventPublisher publisher = new EventPublisher();

    public static EventPublisher getInstance() {
        return publisher;
    }

    private EventPublisher() {
    }
}
