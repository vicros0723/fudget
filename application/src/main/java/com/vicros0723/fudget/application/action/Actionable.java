package com.vicros0723.fudget.application.action;

import com.vicros0723.fudget.domain.account.Account;

import java.io.File;

public interface Actionable {
    class NewFudget implements Actionable {
    }

    class CreateAccount implements Actionable {
        private final Account account;

        public CreateAccount(final Account account) {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }
    }

    class SelectAccount implements Actionable {
        private final Account account;

        public SelectAccount(final Account account) {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }
    }

    class OpenFudget implements Actionable, LastOpenFudgetFilePreferenceSettable {
        private final File file;

        public OpenFudget(final File file) {
            this.file = file;
        }

        @Override
        public File getFile() {
            return file;
        }
    }

    class SaveFudget implements Actionable, LastOpenFudgetFilePreferenceSettable {
        private final File file;

        public SaveFudget(final File file) {
            this.file = file;
        }

        @Override
        public File getFile() {
            return file;
        }
    }
}
