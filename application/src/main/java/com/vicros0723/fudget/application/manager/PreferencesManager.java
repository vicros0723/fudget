package com.vicros0723.fudget.application.manager;

import com.vicros0723.fudget.domain.Fudget;

import java.io.File;
import java.util.prefs.Preferences;

class PreferencesManager {
    static final String LAST_OPEN_FUDGET_FILE_PATH = "lastOpenFudgetFilePath";

    static PreferencesManager getInstance() {
        return new PreferencesManager(Preferences.userNodeForPackage(Fudget.class));
    }

    private final Preferences preferences;

    PreferencesManager(final Preferences preferences) {
        this.preferences = preferences;
    }

    File getLastOpenFudgetFile() {
        return new File(preferences.get(LAST_OPEN_FUDGET_FILE_PATH, ""));
    }

    void setLastOpenFudgetFile(final File file) {
        if (file == null) {
            preferences.remove(LAST_OPEN_FUDGET_FILE_PATH);
        } else {
            preferences.put(LAST_OPEN_FUDGET_FILE_PATH, file.getPath());
        }
    }


}
