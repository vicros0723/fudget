package com.vicros0723.fudget.application.manager;

import com.vicros0723.fudget.application.action.Actionable;
import com.vicros0723.fudget.application.action.LastOpenFudgetFilePreferenceSettable;
import com.vicros0723.fudget.application.event.Event;
import com.vicros0723.fudget.application.event.EventPublisher;
import com.vicros0723.fudget.application.reducer.FudgetReducer;
import com.vicros0723.fudget.domain.Fudget;

public class FudgetManager {
    private static FudgetManager instance;
    static {
        final EventPublisher eventPublisher = EventPublisher.getInstance();
        final FudgetFileManager fudgetFileManager = FudgetFileManager.getInstance();
        final PreferencesManager preferencesManager = PreferencesManager.getInstance();
        final FudgetReducer fudgetReducer = FudgetReducer.getInstance();
        instance = new FudgetManager(Fudget.DEFAULT_FUDGET, eventPublisher, fudgetFileManager, preferencesManager, fudgetReducer);
    }

    public static FudgetManager getInstance() {
        return instance;
    }

    private Fudget fudget;
    private final EventPublisher eventPublisher;
    private final FudgetFileManager fudgetFileManager;
    private final PreferencesManager preferencesManager;
    private final FudgetReducer fudgetReducer;

    FudgetManager(final Fudget fudget,
                  final EventPublisher eventPublisher,
                  final FudgetFileManager fudgetFileManager,
                  final PreferencesManager preferencesManager,
                  final FudgetReducer fudgetReducer) {
        this.fudget = fudget;
        this.eventPublisher = eventPublisher;
        this.fudgetFileManager = fudgetFileManager;
        this.preferencesManager = preferencesManager;
        this.fudgetReducer = fudgetReducer;
    }

    public Fudget getFudget() {
        return fudget;
    }

    public void init() {
        manage(new Actionable.OpenFudget(preferencesManager.getLastOpenFudgetFile()));
    }

    public void manage(final Actionable actionable) {
        try {
            if (actionable instanceof Actionable.SaveFudget) {
                fudgetFileManager.save(((Actionable.SaveFudget) actionable).getFile(), fudget);
            } else if (actionable instanceof Actionable.OpenFudget) {
                fudget = fudgetFileManager.open(((Actionable.OpenFudget) actionable).getFile());
            } else {
                fudget = fudgetReducer.calculateNewFudget(fudget, actionable);
            }

            if (actionable instanceof LastOpenFudgetFilePreferenceSettable) {
                preferencesManager.setLastOpenFudgetFile(((LastOpenFudgetFilePreferenceSettable) actionable).getFile());
            }

            eventPublisher.submit(new Event.Success(fudget));
        } catch (Exception exception) {
            eventPublisher.submit(new Event.Failure(exception));
        }
    }

}
