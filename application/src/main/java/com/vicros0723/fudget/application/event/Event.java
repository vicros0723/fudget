package com.vicros0723.fudget.application.event;

import com.vicros0723.fudget.domain.Fudget;

import java.util.Objects;

// Rename to an adjective
public interface Event {
    class Success implements Event {
        private final Fudget fudget;

        public Success(final Fudget fudget) {
            this.fudget = fudget;
        }

        public Fudget getFudget() {
            return fudget;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Success success = (Success) o;
            return Objects.equals(fudget, success.fudget);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fudget);
        }
    }

    class Failure implements Event {
        private final Exception exception;

        public Failure(final Exception exception) {
            this.exception = exception;
        }

        public Exception getException() {
            return exception;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Failure failure = (Failure) o;
            return Objects.equals(exception, failure.exception);
        }

        @Override
        public int hashCode() {
            return Objects.hash(exception);
        }
    }
}
