package com.vicros0723.fudget.application.reducer;

import com.vicros0723.fudget.application.action.Actionable;
import com.vicros0723.fudget.domain.Fudget;

public class FudgetReducer {
    public static FudgetReducer getInstance() {
        return new FudgetReducer(new AccountsReducer());
    }

    private final AccountsReducer accountsReducer;

    FudgetReducer(final AccountsReducer accountsReducer) {
        this.accountsReducer = accountsReducer;
    }

    public Fudget calculateNewFudget(final Fudget oldFudget, final Actionable actionable) throws Exception {
        return oldFudget
                .withAccounts(accountsReducer.calculateNewAccounts(oldFudget, actionable))
                .withSelectedAccount(accountsReducer.calculateNewSelectedAccount(oldFudget, actionable));
    }
}
