package com.vicros0723.fudget.application.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vicros0723.fudget.application.event.OpenFudgetException;
import com.vicros0723.fudget.application.event.SaveFudgetException;
import com.vicros0723.fudget.domain.Fudget;

import java.io.File;
import java.io.IOException;

class FudgetFileManager {
    private static final FudgetFileManager instance = new FudgetFileManager(new ObjectMapper());

    static FudgetFileManager getInstance() {
        return instance;
    }

    private final ObjectMapper objectMapper;

    FudgetFileManager(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    void save(final File file, final Fudget fudget) {
        try {
            objectMapper.writeValue(file, fudget);
        } catch (IOException exception) {
            throw new SaveFudgetException();
        }
    }

    Fudget open(final File file) {
        try {
            return objectMapper.readValue(file, Fudget.class);
        } catch (IOException exception) {
            throw new OpenFudgetException();
        }
    }
}
