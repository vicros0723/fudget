package com.vicros0723.fudget.application.manager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.File;
import java.util.prefs.Preferences;

import static org.assertj.core.api.Assertions.assertThat;

public class PreferencesManagerTest {
    private PreferencesManager preferencesManager;
    private Preferences preferences;

    @Rule
    public TestName name = new TestName();

    @Before
    public void setUp() {
        preferences = Preferences.userNodeForPackage(PreferencesManagerTest.class);
        preferencesManager = new PreferencesManager(preferences);
    }

    @Test
    public void testGetLastOpenFudgetFile() {
        preferences.put(PreferencesManager.LAST_OPEN_FUDGET_FILE_PATH, name.getMethodName());

        final File lastOpenFudgetFile = preferencesManager.getLastOpenFudgetFile();

        assertThat(lastOpenFudgetFile.getPath()).isEqualTo(name.getMethodName());
    }

    @Test
    public void testGetLastOpenFudgetFileWithNoLastOpenFudgetFilePath() throws Exception {
        preferences.clear();

        final File lastOpenFudgetFile = preferencesManager.getLastOpenFudgetFile();

        assertThat(lastOpenFudgetFile.getPath()).isEqualTo("");
    }

    @Test
    public void testSetLastOpenFudgetFile() {
        preferences.put(PreferencesManager.LAST_OPEN_FUDGET_FILE_PATH, name.getMethodName() + "Old");

        preferencesManager.setLastOpenFudgetFile(new File(name.getMethodName()));

        final String lastOpenFudgetFilePath = preferences.get(PreferencesManager.LAST_OPEN_FUDGET_FILE_PATH, null);
        assertThat(lastOpenFudgetFilePath).isNotNull().isEqualTo(name.getMethodName());
    }

    @Test
    public void testSetLastOpenFudgetFileWithNullFile() {
        preferences.put(PreferencesManager.LAST_OPEN_FUDGET_FILE_PATH, name.getMethodName() + "Old");

        preferencesManager.setLastOpenFudgetFile(null);

        final String lastOpenFudgetFilePath = preferences.get(PreferencesManager.LAST_OPEN_FUDGET_FILE_PATH, null);
        assertThat(lastOpenFudgetFilePath).isNull();
    }
}
