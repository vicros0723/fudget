package com.vicros0723.fudget.application.manager;

import com.vicros0723.fudget.application.action.Actionable;
import com.vicros0723.fudget.application.event.Event;
import com.vicros0723.fudget.application.event.EventPublisher;
import com.vicros0723.fudget.application.event.OpenFudgetException;
import com.vicros0723.fudget.application.event.SaveFudgetException;
import com.vicros0723.fudget.application.reducer.FudgetReducer;
import com.vicros0723.fudget.domain.Fudget;
import com.vicros0723.fudget.domain.account.Account;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class FudgetManagerTest {
    private Fudget fudget;
    private EventPublisher eventPublisher;
    private FudgetFileManager fudgetFileManager;
    private PreferencesManager preferencesManager;
    private FudgetReducer fudgetReducer;
    private FudgetManager fudgetManager;

    @Rule
    public TestName name = new TestName();

    @Before
    public void setUp() {
        fudget = mock(Fudget.class);
        eventPublisher = mock(EventPublisher.class);
        fudgetFileManager = mock(FudgetFileManager.class);
        preferencesManager = mock(PreferencesManager.class);
        fudgetReducer = mock(FudgetReducer.class);

        fudgetManager = new FudgetManager(fudget, eventPublisher, fudgetFileManager, preferencesManager, fudgetReducer);
    }

    @Test
    public void testManageForSaveFudgetActionable() throws Exception {
        final File file = new File(name.getMethodName());
        final Actionable.SaveFudget actionable = new Actionable.SaveFudget(file);

        fudgetManager.manage(actionable);

        verify(fudgetFileManager).save(file, fudget);
        verifyZeroInteractions(fudgetReducer);
        assertThat(fudgetManager.getFudget()).isEqualTo(fudget);
        verify(preferencesManager).setLastOpenFudgetFile(file);
        verify(eventPublisher).submit(new Event.Success(fudget));
    }

    @Test
    public void testManageForSaveFudgetActionableThrowingException() throws Exception {
        final File file = new File(name.getMethodName());
        final Actionable.SaveFudget actionable = new Actionable.SaveFudget(file);

        final SaveFudgetException exception = new SaveFudgetException();
        doThrow(exception).when(fudgetFileManager).save(file, fudget);

        fudgetManager.manage(actionable);

        verifyZeroInteractions(fudgetReducer);
        assertThat(fudgetManager.getFudget()).isEqualTo(fudget);
        verifyZeroInteractions(preferencesManager);
        verify(eventPublisher).submit(new Event.Failure(exception));
    }

    @Test
    public void testManageForOpenFudgetActionable() throws Exception {
        final File file = new File(name.getMethodName());
        final Actionable.OpenFudget actionable = new Actionable.OpenFudget(file);

        final Fudget newFudget = mock(Fudget.class);
        doReturn(newFudget).when(fudgetFileManager).open(file);

        fudgetManager.manage(actionable);

        verifyZeroInteractions(fudgetReducer);
        assertThat(fudgetManager.getFudget()).isEqualTo(newFudget);
        verify(preferencesManager).setLastOpenFudgetFile(file);
        verify(eventPublisher).submit(new Event.Success(newFudget));
    }

    @Test
    public void testManageForOpenFudgetActionableThrowingException() throws Exception {
        final File file = new File(name.getMethodName());
        final Actionable.OpenFudget actionable = new Actionable.OpenFudget(file);

        final OpenFudgetException exception = new OpenFudgetException();
        doThrow(exception).when(fudgetFileManager).open(file);

        fudgetManager.manage(actionable);

        verifyZeroInteractions(fudgetReducer);
        assertThat(fudgetManager.getFudget()).isEqualTo(fudget);
        verifyZeroInteractions(preferencesManager);
        verify(eventPublisher).submit(new Event.Failure(exception));
    }

    @Test
    public void testManageForUnknownActionable() throws Exception {
        final Account account = mock(Account.class);
        final Actionable.CreateAccount actionable = new Actionable.CreateAccount(account);

        final Fudget newFudget = mock(Fudget.class);
        doReturn(newFudget).when(fudgetReducer).calculateNewFudget(fudget, actionable);

        fudgetManager.manage(actionable);

        verifyZeroInteractions(fudgetFileManager);
        assertThat(fudgetManager.getFudget()).isEqualTo(newFudget);
        verifyZeroInteractions(preferencesManager);
        verify(eventPublisher).submit(new Event.Success(newFudget));
    }

    @Test
    public void testManageForUnknownActionableThrowingException() throws Exception {
        final Account account = mock(Account.class);
        final Actionable.CreateAccount actionable = new Actionable.CreateAccount(account);

        final IllegalArgumentException exception = new IllegalArgumentException();
        doThrow(exception).when(fudgetReducer).calculateNewFudget(fudget, actionable);

        fudgetManager.manage(actionable);

        verifyZeroInteractions(fudgetFileManager);
        assertThat(fudgetManager.getFudget()).isEqualTo(fudget);
        verifyZeroInteractions(preferencesManager);
        verify(eventPublisher).submit(new Event.Failure(exception));
    }
}
