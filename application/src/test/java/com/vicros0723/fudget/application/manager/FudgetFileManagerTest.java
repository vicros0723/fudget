package com.vicros0723.fudget.application.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vicros0723.fudget.application.event.OpenFudgetException;
import com.vicros0723.fudget.application.event.SaveFudgetException;
import com.vicros0723.fudget.domain.Fudget;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestName;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class FudgetFileManagerTest {
    private ObjectMapper objectMapper;
    private FudgetFileManager fudgetFileManager;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Rule
    public TestName name = new TestName();

    @Before
    public void setUp() {
        objectMapper = mock(ObjectMapper.class);
        fudgetFileManager = new FudgetFileManager(objectMapper);
    }

    @Test
    public void testSave() throws Exception {
        final File file = new File(name.getMethodName());
        final Fudget fudget = mock(Fudget.class);

        fudgetFileManager.save(file, fudget);

        verify(objectMapper).writeValue(file, fudget);
    }

    @Test
    public void testSaveThrowingException() throws Exception {
        final File file = new File(name.getMethodName());
        final Fudget openFudget = mock(Fudget.class);
        doThrow(new IOException()).when(objectMapper).writeValue(file, openFudget);

        thrown.expect(SaveFudgetException.class);

        fudgetFileManager.save(file, openFudget);
    }

    @Test
    public void testOpen() throws Exception {
        final Fudget savedFudget = mock(Fudget.class);
        final File savedFile = new File(name.getMethodName());
        doReturn(savedFudget).when(objectMapper).readValue(savedFile, Fudget.class);

        final Fudget openedFudget = fudgetFileManager.open(savedFile);

        assertThat(openedFudget).isEqualTo(savedFudget);
    }

    @Test
    public void testOpenThrowingException() throws Exception {
        final File savedFile = new File(name.getMethodName());
        doThrow(new IOException()).when(objectMapper).readValue(savedFile, Fudget.class);

        thrown.expect(OpenFudgetException.class);

        fudgetFileManager.open(savedFile);
    }
}
