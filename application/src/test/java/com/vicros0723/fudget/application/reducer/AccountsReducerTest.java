package com.vicros0723.fudget.application.reducer;

import com.vicros0723.fudget.application.action.Actionable;
import com.vicros0723.fudget.application.reducer.AccountsReducer;
import com.vicros0723.fudget.domain.Fudget;
import com.vicros0723.fudget.domain.account.Account;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.vicros0723.fudget.application.action.Actionable.*;
import static com.vicros0723.fudget.domain.Fudget.DEFAULT_FUDGET;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class AccountsReducerTest {
    private AccountsReducer accountsReducer;

    @Before
    public void setUp() {
        accountsReducer = new AccountsReducer();
    }

    @Test
    public void testCalculateNewAccountsForNewFudgetActionable() {
        final List<Account> newAccounts = accountsReducer.calculateNewAccounts(mock(Fudget.class), new NewFudget());

        assertThat(newAccounts).containsExactlyElementsOf(DEFAULT_FUDGET.getAccounts());
    }

    @Test
    public void testCalculateNewAccountsForCreateAccountActionable() {
        final Account account = mock(Account.class);

        final List<Account> newAccounts = accountsReducer.calculateNewAccounts(mock(Fudget.class), new CreateAccount(account));

        assertThat(newAccounts).containsExactly(account);
    }

    @Test
    public void testCalculateNewAccountsForUnknownActionable() {
        final List<Account> newAccounts = accountsReducer.calculateNewAccounts(mock(Fudget.class), mock(Actionable.class));

        assertThat(newAccounts).isEmpty();
    }

    @Test
    public void testCalculateNewSelectedAccountForNewFudgetActionable() {
        final Account newSelectedAccount = accountsReducer.calculateNewSelectedAccount(mock(Fudget.class), new NewFudget());

        assertThat(newSelectedAccount).isEqualTo(DEFAULT_FUDGET.getSelectedAccount());
    }

    @Test
    public void testCalculateNewSelectedAccountForCreateAccountActionable() {
        final Account account = mock(Account.class);

        final Account newSelectedAccount = accountsReducer.calculateNewSelectedAccount(mock(Fudget.class), new CreateAccount(account));

        assertThat(newSelectedAccount).isEqualTo(account);
    }

    @Test
    public void testCalculateNewSelectedAccountForSelectAccountActionable() {
        final Account account = mock(Account.class);

        final Account newSelectedAccount = accountsReducer.calculateNewSelectedAccount(mock(Fudget.class), new SelectAccount(account));

        assertThat(newSelectedAccount).isEqualTo(account);
    }

    @Test
    public void testCalculateNewSelectedAccountForUnknownActionable() {
        final Account selectedAccount = mock(Account.class);

        final Fudget fudget = mock(Fudget.class);
        when(fudget.getSelectedAccount()).thenReturn(selectedAccount);

        final Account newSelectedAccount = accountsReducer.calculateNewSelectedAccount(fudget, mock(Actionable.class));

        assertThat(newSelectedAccount).isEqualTo(selectedAccount);
    }
}
