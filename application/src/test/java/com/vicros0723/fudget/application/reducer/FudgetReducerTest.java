package com.vicros0723.fudget.application.reducer;

import com.vicros0723.fudget.application.action.Actionable;
import com.vicros0723.fudget.domain.Fudget;
import com.vicros0723.fudget.domain.account.Account;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class FudgetReducerTest {
    private AccountsReducer accountsReducer;
    private FudgetReducer fudgetReducer;

    @Before
    public void setUp() {
        accountsReducer = mock(AccountsReducer.class);
        fudgetReducer = new FudgetReducer(accountsReducer);
    }

    @Test
    public void testCalculateNewFudgetForUnknownActionable() throws Exception {
        final Actionable.CreateAccount actionable = new Actionable.CreateAccount(mock(Account.class));
        final Fudget oldFudget = Fudget.DEFAULT_FUDGET;

        final List<Account> newAccounts = List.of();
        doReturn(newAccounts).when(accountsReducer).calculateNewAccounts(oldFudget, actionable);

        final Account newSelectedAccount = mock(Account.class);
        doReturn(newSelectedAccount).when(accountsReducer).calculateNewSelectedAccount(oldFudget, actionable);

        final Fudget newFudget = fudgetReducer.calculateNewFudget(oldFudget, actionable);

        assertThat(newFudget.getAccounts()).isEqualTo(newAccounts);
        assertThat(newFudget.getSelectedAccount()).isEqualTo(newSelectedAccount);
    }
}
